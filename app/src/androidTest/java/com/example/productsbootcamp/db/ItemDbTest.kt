package com.example.productsbootcamp.db

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.productsbootcamp.models.Item
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ItemDbTest {

    private lateinit var db: ItemDatabase
    private lateinit var dao: ItemDao
    private lateinit var item: Item


    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()


    @Before
    fun setUp() {
        item = Item(1, "Ship", "Item 1", "10")
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, ItemDatabase::class.java).allowMainThreadQueries()
            .build()
        dao = db.itemDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertItemTest() = runBlocking {
        dao.addItem(item)
        Assert.assertEquals(1, dao.getItems().size)
    }

    @Test
    fun getItemsTest() = runBlocking {
        dao.addItem(item)
        val item2 = Item(2, "Ship", "Item 2", "100")
        dao.addItem(item2)
        val list = listOf<Item>(item, item2)
        Assert.assertEquals(list, dao.getItems())
    }

    @Test
    fun getItemByNameTest() = runBlocking{
        dao.addItem(item)
        Assert.assertEquals(item, dao.getItemByName("Item 1"))
    }

    @Test
    fun deleteItemTest() = runBlocking {
        dao.addItem(item)
        dao.deleteItem(item)
        Assert.assertEquals(0, dao.getItems().size)
    }

    @Test
    fun updateItemTest() = runBlocking {
        dao.addItem(item)
        val updatedItem = Item(1, null, "Updated Item", "10")
        dao.updateItem(updatedItem)
        Assert.assertEquals(1, dao.getItems().size)
        Assert.assertEquals(updatedItem, dao.getItemByName("Updated Item"))
    }

}