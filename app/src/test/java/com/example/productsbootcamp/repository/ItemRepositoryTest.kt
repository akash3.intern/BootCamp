package com.example.productsbootcamp.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.example.productsbootcamp.api.ItemService
import com.example.productsbootcamp.db.ItemDao
import com.example.productsbootcamp.db.ItemDatabase
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.models.ItemList
import com.example.productsbootcamp.utils.NetworkUtils
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import net.bytebuddy.matcher.ElementMatchers.any
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
class ItemRepositoryTest {
    private lateinit var itemRepository: ItemRepository
    private lateinit var itemService: ItemService
    private lateinit var itemDatabase: ItemDatabase
    private lateinit var itemDao: ItemDao
    private lateinit var context: Context

    @Before
    fun setUp() {
        itemService = mockk()
        itemDao = mockk()
        itemDatabase = mockk(relaxed = true)
        context = mockk(relaxed = true)
        itemRepository = spyk(ItemRepository(itemService, itemDatabase, context))
        every { itemDatabase.itemDao() } returns itemDao
    }

    @Test
    fun `internet available and sync db called`() = runBlocking {
        val response = mockk<Response<ItemList>>()
        val itemList = mockk<ItemList>(relaxed = true)
        every { response.body() } returns itemList
        coEvery { itemService.getItems() } returns response
        every { NetworkUtils.isInternetAvailable(context) } returns true
        itemRepository.syncDatabase()
        coVerify(exactly = 1) { itemRepository.updateDatabase(any()) }
    }

    @Test
    fun `internet unavailable and sync db called`() = runBlocking {
        val response = mockk<Response<ItemList>>()
        val itemList = mockk<ItemList>(relaxed = true)
        every { response.body() } returns itemList
        coEvery { itemService.getItems() } returns response
        every { NetworkUtils.isInternetAvailable(context) } returns false
        itemRepository.syncDatabase()
        coVerify(exactly = 0) { itemRepository.updateDatabase(any()) }
    }

    @Test
    fun `update db with item already present`() = runBlocking {
        val item = Item(1, "Ship", "Item 1", "10")
        val list = listOf<Item>(item)
        coEvery { itemDao.getItemByName(any()) } returns null
        itemRepository.updateDatabase(list)
        coEvery { itemDao.addItem(any()) }
        coVerify(exactly = 1)  {itemRepository.updateDatabase(any())}
        coVerify(exactly = 1) { itemDao.getItemByName(any()) }
    }

    @Test
    fun `get live data`() = runBlocking {
        val item = Item(1, "Ship", "Item 1", "10")
        val list = listOf<Item>(item)
        coEvery { itemDao.getItems() } returns list
        itemRepository.getItems()
        coVerify(exactly = 1) { itemDao.getItems() }
        assertEquals(list, itemRepository.items.value)
    }
}