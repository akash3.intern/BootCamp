package com.example.productsbootcamp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.productsbootcamp.api.ItemService
import com.example.productsbootcamp.db.ItemDao
import com.example.productsbootcamp.db.ItemDatabase
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.repository.ItemRepository
import com.example.productsbootcamp.viewModels.MainViewModel
import io.mockk.*
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.manipulation.Ordering.Context

class MainViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var itemRepository: ItemRepository
    private lateinit var viewModel: MainViewModel
    private var liveData = MutableLiveData<List<Item>>()

    @Before
    fun setUp() {
        itemRepository = mockk(relaxed = true)
        viewModel  = spyk(MainViewModel(itemRepository))
    }

    @Test
    fun `init block calling repo functions`() {
        coVerify(exactly = 1) { itemRepository.syncDatabase() }
        coVerify(exactly = 1) { itemRepository.getItems() }
    }

    @Test
    fun `get live data`(){
        val item = Item(1, "Ship", "Item 1", "10")
        val list = listOf<Item>(item)
        liveData.postValue(list)
        every { itemRepository.items }  returns liveData
        assertEquals(liveData, viewModel.items)
    }

}