package com.example.productsbootcamp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.example.productsbootcamp.api.ItemService
import com.example.productsbootcamp.api.RetrofitHelper
import com.example.productsbootcamp.databinding.ActivityMainBinding
import com.example.productsbootcamp.fragments.Fragment1
import com.example.productsbootcamp.fragments.Fragment2
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.repository.ItemRepository
import com.example.productsbootcamp.viewModels.MainViewModel
import com.example.productsbootcamp.viewModels.MainViewModelFactory
import com.google.android.material.navigation.NavigationBarMenu
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainViewModel : MainViewModel
    private var previousMenuItem: MenuItem? = null
    private var itemList = ArrayList<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



        val repository = (application as ItemApplication).itemRepository
        mainViewModel = ViewModelProvider(this, MainViewModelFactory(repository))[MainViewModel::class.java]

        setupSearch()
        setupViewPager()
        setupBottomNav()

    }

    private fun setupSearch() {
        binding.search.addTextChangedListener(object: TextWatcher  {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                filter(p0.toString())
            }
        })
    }

    private fun filter(text: String) {
        val filteredList= ArrayList<Item>()
        mainViewModel.items.value?.forEach {
            if (it.name.toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(it)
            }
        }

        if(binding.viewpager.currentItem == 0){
            val frag = supportFragmentManager.findFragmentByTag("f" + binding.viewpager.currentItem) as Fragment1
            frag.filter(filteredList)
        } else{
            val frag = supportFragmentManager.findFragmentByTag("f" + binding.viewpager.currentItem) as Fragment2
            frag.filter(filteredList)
        }
    }

    private fun setupBottomNav() {
        binding.bottomNav.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener {
            when(it.itemId){
                R.id.first -> binding.viewpager.currentItem = 0
                R.id.second -> binding.viewpager.currentItem = 1
            }
            true
        })
    }


    private fun setupViewPager() {

        val viewPager = binding.viewpager
        val fragments: List<Fragment> = listOf(
            Fragment1(),
            Fragment2()
        )
        val adapter = ViewPagerAdapter(fragments, this)
        viewPager.adapter = adapter

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                if (previousMenuItem != null) {
                    previousMenuItem?.isChecked = false
                }
                else {
                    binding.bottomNav.menu.getItem(0).isChecked = false
                }
                binding.bottomNav.menu.getItem(position).isChecked = true
                previousMenuItem = binding.bottomNav.menu.getItem(position)
            }
        })

    }
}
