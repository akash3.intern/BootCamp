package com.example.productsbootcamp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.productsbootcamp.databinding.GridItemBinding
import com.example.productsbootcamp.databinding.ListItemBinding
import com.example.productsbootcamp.models.Item

class ItemGridAdapter() : RecyclerView.Adapter<GridItemViewHolder>() {

    private val items= ArrayList<Item>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridItemViewHolder {
        val view = GridItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GridItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: GridItemViewHolder, position: Int) {
        val currentItem = items[position]
        holder.titleView.text = currentItem.name
        holder.priceView.text = currentItem.price
        holder.image.setImageResource(R.drawable.color_rectangle)
    }

    fun updateItem(updateItems: List<Item>){
        items.clear()
        items.addAll(updateItems)
        notifyDataSetChanged()
    }

}

class GridItemViewHolder(private val binding: GridItemBinding) : RecyclerView.ViewHolder(binding.root){
    val titleView = binding.cardTitle
    val priceView = binding.cardPrice
    val image = binding.cardImage
}
