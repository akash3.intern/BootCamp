package com.example.productsbootcamp.fragments

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.productsbootcamp.ItemGridAdapter
import com.example.productsbootcamp.ItemListAdapter
import com.example.productsbootcamp.R
import com.example.productsbootcamp.databinding.Fragment1Binding
import com.example.productsbootcamp.databinding.Fragment2Binding
import com.example.productsbootcamp.models.Item
import com.example.productsbootcamp.viewModels.MainViewModel

class Fragment2 : Fragment() {

    private lateinit var binding: Fragment2Binding
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: ItemGridAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java]
        binding = Fragment2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerview.layoutManager = GridLayoutManager(context, 3)
        adapter = ItemGridAdapter()
        binding.recyclerview.adapter = adapter
        viewModel.items.observe(viewLifecycleOwner) {
            adapter.updateItem(it)
        }
    }

    fun filter(list: List<Item>) {
        adapter.updateItem(list)
    }
}