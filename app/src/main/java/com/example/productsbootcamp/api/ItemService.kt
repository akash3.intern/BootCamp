package com.example.productsbootcamp.api

import com.example.productsbootcamp.models.ItemList
import retrofit2.Response
import retrofit2.http.GET

interface ItemService {

    @GET("/v3/b6a30bb0-140f-4966-8608-1dc35fa1fadc")
    suspend fun getItems(): Response<ItemList>
}


