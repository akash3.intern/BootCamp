package com.example.productsbootcamp.models

data class ItemList(
    val `data`: Data,
    val error: Any,
    val status: String
)