package com.example.productsbootcamp.models

data class Data(
    val items: List<Item>
)